<?php

namespace Ocw\RequestCaseConverter;

use Illuminate\Support\ServiceProvider;
use Ocw\RequestCaseConverter\RequestCaseConverter;

class RequestCaseConverterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('request-case-converter', function($app) {
            return new RequestCaseConverter();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
