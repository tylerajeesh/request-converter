<?php

namespace Ocw\RequestCaseConverter;

use Illuminate\Support\Str;

class RequestCaseConverter
{
    public $request;
    public $skip = [];
    public $only = [];

    public function __construct()
    {
        $this->request = request();
    }

    public function make()
    {
        $replaced = [];
        $keys = [];
        foreach ($this->request->all() as $key => $value) {
            if(!empty($this->only)){
                if(in_array($key, $this->only)){
                    $replaced[Str::snake($key)] = $value;
                    $keys[] = $key;
                }
            }
            else if (!in_array($key, $this->skip)){
                $replaced[Str::snake($key)] = $value;
                $keys[] = $key;
            }
            else {
                $replaced[$key] = $value;
            }
        }
        //$keys = array_keys($replaced);
        foreach($keys as $k)
           $this->request->request->remove($k);

        $this->add($replaced);
        //return $this;
    }

    public function toArray()
    {
        $this->make();
        return $this->request->all();
    }

    public function skip($array = [])
    {
        $this->skip = $array;
        return $this;
    }

    public function add($array = []){
        $this->request->merge($array);
        return $this;
    }

    public function only($array = []){
        $this->only = $array;
        return $this;
    }
}
