<?php

namespace Ocw\RequestCaseConverter\Facades;

use Illuminate\Support\Facades\Facade;

class RequestCaseConverter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'request-case-converter';
    }
}
